---
title: Founding Partners
description: We aim to raise non-equity dilutive capital by offering 'Founding Partner' Sponsorship Packages.
category: Revenue Channels
position: 2001
---

## Overview

We aim to raise non-equity dilutive capital by offering 'Founding Partner' Sponsorship Packages to Angel Investors and
VCs.

<cta-button link="https://grey.software/pitch" text="Current Pitch"></cta-button>

### Value Proposition

**For** Angel investors and VC firms

**Who are dissatisfied** with how the status quo is not optimally preparing the next generation to create or scale
software products

**Grey Software** is making open software products and publishing what we learn along the way in an organized education
ecosystem

**So that** young, aspiring software entrepreneurs around the world can have access to authentic, up-to-date, and
practical software education

**Unlike** the confusing web of outdated university curriculums, selective internships, expensive bootcamps or online
courses, and free but unorganized resources

## Impact & Traction

### Malpani Ventures Grant

- Dr. Malpani from Malpani ventures authorized two grants totaling 130,000 INR after he announced he was funding
  open-source ed-tech ventures in India

### Open Core Summit Grant

- Joseph Jacks from the Open Core Summit gave us a \$1000 CAD grant after we made our case on Twitter and LinkedIn

## Risk

### What are the key risks that would be involved in making this investment?

- After using all the money from its founding investments, the organization is still unable to sustain itself through
  its revenue channels.

### What plans are in place to mitigate these risks in the next period?

- Public weekly and milestone updates
